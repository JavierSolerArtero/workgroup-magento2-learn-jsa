# Assignment A

Example:
```
Magento\Quote\Model\ValidationRules\QuoteValidationComposite
```
### 1. How the composite class makes sure leaves are validated
- With the instance validation in constructor.

![Assignment](./images/Assignment-A.png)

- This way of validation can be moved when traversing the handler array. This should be evaluated, as the constructor is not usually used to add anything other than assignments as it can affect performance, but if the handlers are traversed it would often be convenient to leave it in the constructor.

### 2. What is the business logic of the class? What does it try to Compose ?
- Tries to decompose the validation of the quote, so that it is easy to add new validations by extending the handlers.

![Assignment](./images/validate.png)

- In this way, for each validation its result is iterated and checked if the result is valid. If it is, is pushed to the returned array


### 3. Identify the client that uses this Composite

![Assignment](./images/clients.png)

Clients (in order):

- module-quote:
```
<item name="AllowedCountryValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\AllowedCountryValidationRule</item>
<item name="ShippingAddressValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\ShippingAddressValidationRule</item>
<item name="ShippingMethodValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\ShippingMethodValidationRule</item>
<item name="BillingAddressValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\BillingAddressValidationRule</item>
<item name="PaymentMethodValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\PaymentMethodValidationRule</item>
<item name="MinimumAmountValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\MinimumAmountValidationRule</item>
```

- module-inventory-in-store-pickup-quote:
```
<item name="InStorePickupQuoteValidationRule" xsi:type="object">Magento\InventoryInStorePickupQuote\Model\Quote\ValidationRule\InStorePickupQuoteValidationRule</item>
<item name="InStorePickupQuoteValidationRule" xsi:type="object">Magento\InventoryInStorePickupQuote\Model\Quote\ValidationRule\InStorePickupQuoteValidationRule</item>
```

- module-quote (2):
```
<item name="ShippingAddressValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\ShippingAddressValidationRule</item>
<item name="AllowedCountryValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\AllowedCountryValidationRule</item>
<item name="ShippingMethodValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\ShippingMethodValidationRule</item>
<item name="BillingAddressValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\BillingAddressValidationRule</item>
<item name="PaymentMethodValidationRule" xsi:type="object">Magento\Quote\Model\ValidationRules\PaymentMethodValidationRule</item>
```

### 4. Draw a class diagram. Is it different than the diagram from 1. The Composite pattern.

# Assignment B

Example:
```
Magento\Payment\Gateway\Request\BuilderComposite
``` 

### 1. Identify the third party vendors that use this class

### 2. Analyse code from this virtual type 

```virtualType name="BraintreeAuthorizeRequest"```

Are there different builders used when the area is adminhtml ?

Frontend: 
![Assignment](./images/Assignment-B-1.png)

Backend:
![Assignment](./images/Assignment-B-2.png)

Yes, it is different and does not affect its implementation.

# Assignment C

Example: 

``` Rocket\AutorelatedProducts\Model\Indexer\Autorelated\ProductCollection\CompositeCollectionHandler ``` 

### 1. Edit in a such a way that it will make sure that all of the Leaves are of CollectionHandlerInterface

```
/**
     * @inheritDoc
     */
    public function updateCollection(Collection $collection, array $params): Collection
    {
        foreach ($this->handlers as $handler) {
            if($handler instance of CollectionHandlerInterface) {
                $handler->updateCollection($collection, $params);
            }
        }

        return $collection;
    }
```
** Maybe this is not the best option if this method is called a lot of times. If it is the case, is better to put this conditional in the constructor. **

### 2. Support the CompositeCollectionHandler with automated integration tests

# Assignment D

### 1. Draw the class diagram of the ```CollectionHandlerInterface``` from Autorelated module

![Assignment](./images/diagram.png)

- All these handlers are implementations of ```CollectionHandlerInterface```

### 2. Compare this diagram with the diagrams attached at  1. The Composite pattern . What is the difference?