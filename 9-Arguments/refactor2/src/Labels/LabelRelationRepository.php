<?php

namespace src\Labels;

use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Rocket\Index\Api\Indexer\LookupInterface;
use Rocket\Labels\Api\Data\LabelInterface;
use Rocket\Labels\Api\Data\LabelRelationInterface;
use Rocket\Labels\Model\LabelFactory;
use Rocket\Labels\Model\ResourceModel\Label as LabelResource;
use src\Labels\Builder\LabelBuilder;

/**
 * Implementation of LabelRepositoryInterface
 */
class LabelRelationRepository implements LabelRelationRepositoryInterface
{
    /**
     * @var LookupInterface
     */
    private $lookup;

    /**
     * @var LabelResource
     */
    private $labelResource;

    /**
     * @var LabelFactory
     */
    private $labelFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LookupInterface $lookup
     * @param LabelResource $labelResource
     * @param LabelFactory $labelFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        LookupInterface $lookup,
        LabelResource   $labelResource,
        LabelFactory    $labelFactory,
        LoggerInterface $logger
    )
    {
        $this->lookup = $lookup;
        $this->labelResource = $labelResource;
        $this->labelFactory = $labelFactory;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function getLabels
    (
        int $productId,
        int $websiteId,
        int $position = null,
        int $verticalAlign = null,
        int $horizontalAlign = null,array $labelBuilder): array
    {
        $labelIds = [];
        $arguments = [
            LabelRelationInterface::PRODUCT_ID => $labelBuilder['productId'],
            LabelRelationInterface::WEBSITE_ID => $labelBuilder['websiteId']
        ];

        try {
            $labelsRawData = $this->lookup->findMany($arguments);
            $labelIds = $this->getLabelIds($labelsRawData);
        } catch (LocalizedException $e) {
            $this->logger->error('Lookup find many labels resulted in: ' . $e->getMessage());
        }

        $labels = $this->prepareLabels($labelIds);

        if ($labelBuilder['position']) {
            $labels = $this->filterByPosition($labels, $labelBuilder['position']);
        }

        if ($labelBuilder['verticalAlign']) {
            $labels = $this->filterByVerticalAlign($labels, $labelBuilder['verticalAlign']);
        }

        if ($labelBuilder['horizontalAlign']) {
            $labels = $this->filterByHorizontalAlign($labels, $labelBuilder['horizontalAlign']);
        }

        return $labels;
    }

    /**
     * @param array $labelsRawData
     * @return array
     */
    private function getLabelIds(array $labelsRawData): array
    {
        $result = [];

        foreach ($labelsRawData as $labelRaw) {
            $result[] = $labelRaw[LabelRelationInterface::LABEL_ID];
        }

        return $result;
    }

    /**
     * @param array $labelIds
     * @return LabelInterface[]
     */
    private function prepareLabels(array $labelIds): array
    {
        $result = [];

        foreach ($labelIds as $labelId) {
            $label = $this->labelFactory->create();
            $this->labelResource->load($label, $labelId);
            $result[] = $label;
        }

        return $result;
    }

    /**
     * @param array $labels
     * @param int $position
     * @return array
     */
    private function filterByPosition(array $labels, int $position): array
    {
        return array_filter($labels, function ($label) use ($position) {
            /** @var LabelInterface $label */
            $positions = explode(',', $label->getPosition());

            return in_array($position, $positions);
        });
    }

    /**
     * @param array $labels
     * @param int $verticalAlign
     * @return array
     */
    private function filterByVerticalAlign(array $labels, int $verticalAlign): array
    {
        return array_filter($labels, function ($label) use ($verticalAlign) {
            /** @var LabelInterface $label */
            $positions = explode(',', $label->getVerticalAlign());

            return in_array($verticalAlign, $positions);
        });
    }

    /**
     * @param array $labels
     * @param int $horizontalAlign
     * @return array
     */
    private function filterByHorizontalAlign(array $labels, int $horizontalAlign): array
    {
        return array_filter($labels, function ($label) use ($horizontalAlign) {
            /** @var LabelInterface $label */
            $positions = explode(',', $label->getHorizontalAlign());

            return in_array($horizontalAlign, $positions);
        });
    }
}
