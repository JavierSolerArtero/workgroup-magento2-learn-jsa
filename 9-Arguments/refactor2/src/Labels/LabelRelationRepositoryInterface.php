<?php

namespace src\Labels;

use Rocket\Labels\Api\Data\LabelInterface;
use src\Labels\Builder\LabelBuilder;

/**
 * Interface LabelRelationRepository
 */
interface LabelRelationRepositoryInterface
{
    /**
     * Get the labels that are matched for the given product in the given website
     *
     * @param array $labelBuilder
     * @return array
     */
    public function getLabels(array $labelBuilder): array;
}
