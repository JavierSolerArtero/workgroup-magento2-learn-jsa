<?php

namespace src\Labels\Builder;

class LabelBuilder
{

    /**
     * @var int
     */
    private $productId;

    /**
     * @var int
     */
    private $websiteId;

    /**
     * @var int
     */
    private $position;

    /**
     * @var int
     */
    private $verticalAlign;

    /**
     * @var int
     */
    private $horizontalAlign;

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getWebsiteId()
    {
        return $this->websiteId;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return int
     */
    public function getVerticalAlign()
    {
        return $this->verticalAlign;
    }

    public function getHorizontalAlign()
    {
        return $this->horizontalAlign;
    }


    /**
     * @param int $productId
     * @return void
     */
    public function setProductId(int $productId)
    {
        $this->productId = $productId;
    }

    /**
     * @param int $websiteId
     * @return void
     */
    public function setWebsiteId(int $websiteId)
    {
        $this->websiteId = $websiteId;
    }

    /**
     * @param int $position
     * @return void
     */
    public function setPosition(int $position)
    {
        $this->position = $position;
    }

    /**
     * @param int $verticalAlign
     * @return void
     */
    public function setVerticalAlign(int $verticalAlign)
    {
        $this->verticalAlign = $verticalAlign;
    }

    /**
     * @param int $horizontalAlign
     * @return void
     */
    public function setHorizontalAlign(int $horizontalAlign)
    {
        $this->horizontalAlign = $horizontalAlign;
    }

}