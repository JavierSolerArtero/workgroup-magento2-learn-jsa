<?php

namespace src;

use src\Labels\Builder\LabelBuilder;
use src\Labels\LabelRelationRepositoryInterface;

class LabelClient
{

    /**
     * @var LabelRelationRepositoryInterface
     */
    private $labelRelationRepository;


    public function __construct(LabelRelationRepositoryInterface $labelRelationRepository)
    {
        $this->labelRelationRepository = $labelRelationRepository;
    }

    public function useGetLabels()
    {
        $productId = 1;
        $websiteId = 1;
        $position = 1;
        $verticalAlign = 1;
        $horizontalAlign = 1;

        $labelBuilder = [
            'productId' => $productId,
            'websiteId' => $websiteId,
            'position' => $position,
            'verticalAlign' => $verticalAlign,
            'horizontalAlign' => $horizontalAlign,
        ];

        $this->labelRelationRepository->getLabels($labelBuilder);
    }

}