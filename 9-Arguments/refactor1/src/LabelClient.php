<?php

namespace src;

use src\Labels\Builder\LabelBuilder;
use src\Labels\LabelRelationRepositoryInterface;

class LabelClient
{

    /**
     * @var LabelRelationRepositoryInterface
     */
    private $labelRelationRepository;

    /**
     * @var LabelBuilder
     */
    private $labelBuilder;

    public function __construct(LabelRelationRepositoryInterface $labelRelationRepository)
    {
        $this->labelRelationRepository = $labelRelationRepository;
        $this->labelBuilder = new LabelBuilder();
    }

    public function useGetLabels()
    {
        $productId = 1;
        $websiteId = 1;
        $position = 1;
        $verticalAlign = 1;
        $horizontalAlign = 1;

        $this->labelBuilder->setProductId($productId);
        $this->labelBuilder->setWebsiteId($websiteId);
        $this->labelBuilder->setPosition($position);
        $this->labelBuilder->setVerticalAlign($verticalAlign);
        $this->labelBuilder->setHorizontalAlign($horizontalAlign);

        $this->labelRelationRepository->getLabels($this->labelBuilder);
    }

}