# A.
### Observe the Repository Interfaces from the examples

# B.
### The Significance of the Api  namespace
- API: Application Programming Interface, is a set of interfaces and their implementations that a module provides to other modules.

# C.
### What are the minimum required methods for an Interface to be claimed Repository in Magento?

# D.
### Explain the signatures of all the Repository’s methods: #param, #return, #throws

# E.
### In which areas is the Repository used: adminhtml, frontend, crontab, graphql … etc
- In places that are CRUD operations