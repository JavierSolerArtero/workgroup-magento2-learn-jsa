# Rocket Terms manager 

This module allows from the administrator we will register the checks to accept and the sections to show.
##Backend

#####Listing
From the main menu, visit `Rocket > Content > Terms and Conditions`. A grid listing of Terms and Conditions appers.

#####Edit

- General Information

        a. Name (only information)
        b. Checkbox Text
        c. Is active  (Yes, no)
        d. Required  (Yes, no)
        e. Default  (Yes, no)
        g. Area (Block Newsletter, Customer Form Register)
        h. Stores 
        
###New Area
file: etc/di.xml
```
    <type name="Rocket\TermsManager\Model\Source\LayoutContainer">
        <arguments>
            <argument name="layouts" xsi:type="array">
                <item name="subscribe" xsi:type="string">Block Newsletter</item>
                <item name="customer_form_register" xsi:type="string">Customer Form Register</item>
            </argument>
        </arguments>
    </type>
```
###Show checkbox
 ```
$terms = $viewModels->require(Terms::class);

...
<?=  $terms->showCheckBox($block->getNameInLayout());?>

```
