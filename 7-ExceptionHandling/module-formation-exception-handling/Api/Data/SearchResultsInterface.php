<?php

namespace Rocket\FormationExceptionHandling\Api\Data;

use Rocket\FormationExceptionHandling\Api\Data\FormationLabelInterface;

interface SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return FormationLabelInterface[]
     */
    public function getItems(): array;

    /**
     * Set attributes list.
     *
     * @param FormationLabelInterface[] $items
     * @return $this
     */
    public function setItems(array $items): SearchResultsInterface;
}
