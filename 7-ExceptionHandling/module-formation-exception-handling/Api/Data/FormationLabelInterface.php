<?php

namespace Rocket\FormationExceptionHandling\Api\Data;

/**
 * Interface FormationLabelInterface
 */
interface FormationLabelInterface
{

    const ENTITY_ID = 'entity_id';

    const NAME = 'name';

    const DESCRIPTION = 'description';

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return FormationLabelInterface
     */
    public function setName(string $name): FormationLabelInterface;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @param string $description
     * @return FormationLabelInterface
     */
    public function setDescription(string $description): FormationLabelInterface;

}
