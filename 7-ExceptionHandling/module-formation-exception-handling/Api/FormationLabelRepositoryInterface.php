<?php

namespace Rocket\FormationExceptionHandling\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Rocket\FormationExceptionHandling\Api\Data\FormationLabelInterface;
use Rocket\FormationExceptionHandling\Api\Data\SearchResultsInterface;

/**
 * Interface TermRepositoryInterface
 */
interface FormationLabelRepositoryInterface
{

    /**
     * Save a object
     *
     * @param FormationLabelInterface $label
     * @return FormationLabelInterface
     * @throws CouldNotSaveException
     */
    public function save(FormationLabelInterface $label): FormationLabelInterface;

    /**
     * Retrieve import data which match a specified criteria.
     *
     * @param SearchCriteriaInterface $criteria
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria): SearchResultsInterface;

    /**
     * @param FormationLabelInterface $label
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(FormationLabelInterface $label): bool;
}
