<?php

namespace Rocket\FormationExceptionHandling\Model;

use Magento\Framework\Model\AbstractModel;
use Rocket\FormationExceptionHandling\Api\Data\FormationLabelInterface;
use Rocket\FormationExceptionHandling\Model\ResourceModel\FormationLabel as ResourceModel;

class FormationLabel extends AbstractModel implements FormationLabelInterface
{
    const CACHE_TAG = 'formation_labels';   // identificador único para usarlo dentro del almacenamiento de caché

    public function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    public function getName(): string
    {
        return $this->getData(self::NAME);
    }

    public function setName(string $name): FormationLabelInterface
    {
        return $this->setData(self::NAME, $name);
    }

    public function getDescription(): string
    {
        return $this->getData(self::DESCRIPTION);
    }

    public function setDescription(string $description): FormationLabelInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }
}
