<?php

namespace Rocket\FormationExceptionHandling\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Rocket\FormationExceptionHandling\Api\Data\FormationLabelInterface;
use Rocket\FormationExceptionHandling\Api\Data\SearchResultsInterface;
use Rocket\FormationExceptionHandling\Api\FormationLabelRepositoryInterface;
use Rocket\FormationExceptionHandling\Model\ResourceModel\FormationLabel as LabelResource;
use Rocket\FormationExceptionHandling\Model\ResourceModel\FormationLabel\CollectionFactory;


class FormationLabelRepository implements FormationLabelRepositoryInterface
{

    /**
     * @var LabelResource
     */
    private $labelResource;

    private $searchResultsFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    public function __construct(LabelResource $labelResource, CollectionFactory $collectionFactory, CollectionProcessorInterface $collectionProcessor)
    {
        $this->labelResource = $labelResource;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param FormationLabelInterface $label
     * @return FormationLabelInterface
     * @throws CouldNotSaveException
     */
    public function save(FormationLabelInterface $label): FormationLabelInterface
    {
        try {
            $this->labelResource->save($label);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }

        return $label;
    }

    /**
     * @param SearchCriteriaInterface $criteria
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria): SearchResultsInterface
    {
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @param FormationLabelInterface $label
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(FormationLabelInterface $label): bool
    {
        try {
            $this->labelResource->delete($label);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }
}
