![Diagram](./imgs/img1.png)

### Oversimplified 
High-level modules should not import anything from low-level modules. Both should depend on abstractions (e.g., interfaces).

Abstractions should not depend on details. Details (concrete implementations) should depend on abstractions.

## Assignments

### 1. Compare these two classes
```
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Product data provider
 */
class ProductDataProvider
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Get product data by id
     *
     * @param int $productId
     * @return array
     */
    public function getProductDataById(int $productId): array
    {
        $product = $this->productRepository->getById($productId);
        $productData = $product->toArray();
        $productData['model'] = $product;
        return $productData;
    }
}
```
```
use Magento\Catalog\Block\Product\ImageFactory;
use Magento\Catalog\CustomerData\CompareProducts;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\NoSuchEntityException;

class AddImages
{
    /**
     * @var ImageFactory
     */
    private $imageFactory;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ImageFactory $imageFactory, ProductRepository $productRepository)
    {
        $this->imageFactory = $imageFactory;
        $this->productRepository = $productRepository;
    }

    /**
     * @param CompareProducts $subject
     * @param array $result
     * @return array
     * @see CompareProducts::getSectionData
     */
    public function afterGetSectionData(CompareProducts $subject, array $result): array
    {
        /** @var array $compareItem */
        foreach ($result['items'] as &$compareItem) {
            try {
                /** @var Product $product */
                $product = $this->productRepository->getById((int)$compareItem['id']);
                $image = $this->imageFactory->create($product, 'product_comparison_list');
                $compareItem['image'] = $image->getImageUrl();
            } catch (NoSuchEntityException $exception) {
                continue;
            }
        }

        return $result;
    }
}
```
Both need to execute

```
$product = $this->productRepository->getById($someId);

```
But their constructors are different. 

Keeping program extensibility and maintainability in mind, which way of injection would be a better long term choice?

### Answer:

The best way of injection will be via an interface, as it allows decoupling of the ``MagentoCatalogModelProductRepository`` dependency. Because if it were to change at some point, the implementation and the treatment of the value returned from it would also have to change. Otherwise, if there were changes, you would only have to change the implementation of the class that implements the interface.

### 2. From AddImages lets examine
```
use Magento\Catalog\Block\Product\ImageFactory;
use Magento\Catalog\CustomerData\CompareProducts;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\NoSuchEntityException;
*
*
*
/** @var Product $product */
$product = $this->productRepository->getById((int)$compareItem['id']);
$image = $this->imageFactory->create($product, 'product_comparison_list');
```

Can we be sure that ```$this->productRepository->getById``` will always return ```Magento\Catalog\Model\Product``` ?
- No, it returns ```Magento\Catalog\Api\Data\ProductInterface``` and throws ```Magento\Framework\Exception\NoSuchEntityException``` if it is not found

Propose a way to refactor the code so that 

- ```$this->imageFactory->create``` actually receives ```Magento\Catalog\Model\Product``` as expected

- The AddImages does not have code like use ```Magento\Catalog\Model\Product;```

```
use Magento\Catalog\Block\Product\ImageFactory;
use Magento\Catalog\CustomerData\CompareProducts;
use Magento\Catalog\Api\Data\ProductInterface as Product;
use Magento\Catalog\Api\ProductRepositoryInterface as ProductRepository;
use Magento\Framework\Exception\NoSuchEntityException;

*
*   ...
*

try {
    /** @var Product $product */
    $product = $this->productRepository->getById((int)$compareItem['id']);
    $image = $this->imageFactory->create($product, 'product_comparison_list');
}
catch(NoSuchEntityException $e) {

}
```