## ```module-catalog-products-autorelated/Model/Indexer/Autorelated/RelationProcessor.php```

### We had
```
$products = $this->productCollectionFactory->create()
    ->addAttributeToSelect(array_keys($this->systemManagement->getAttributeCodes()))
    ->addFieldToFilter('entity_id', ['in' => $productIds])
    ->addStoreFilter($storeId)
    ->getItems();
```
### What we could have done (but did not do)
```
$products = $this->productCollectionFactory->create()
    ->addAttributeToSelect(array_keys($this->systemManagement->getAttributeCodes()))
    ->addFieldToFilter('entity_id', ['in' => $productIds])
    ->addStoreFilter($storeId)
    ->getItems();
```

## Assignment

### 1. What is the benefit of refactoring the code to be like
The filtering functionality has been delegated to an interface i to the dependency injector, and not to a single method. Thus fulfilling the open close principle and the dependency inversion principle

### 2. Do a sample code with this requirement: skip processing out of stock products. How would the ```RelationProcessor``` class look like if we continued with the old code?
We would have to make a new implementation of the ```CollectionHandlerInterface``:
```
namespace Rocket\AutorelatedProducts\Model\Indexer\Autorelated\ProductCollection\Handlers;

class OutOfStock implements CollectionHandlerInterface
{
    /**
     * @inheritDoc
     */
    public function updateCollection(Collection $collection, array $params): Collection
    {
        // out of stock filtering

        return $collection;
    }
}
```
And add to di.xml in the handlers' array

<type name="Rocket\AutorelatedProducts\Model\Indexer\Autorelated\ProductCollection\CompositeCollectionHandler">

        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="store" xsi:type="object">Rocket\AutorelatedProducts\Model\Indexer\Autorelated\ProductCollection\Handlers\Store</item>
                <item name="ids" xsi:type="object">Rocket\AutorelatedProducts\Model\Indexer\Autorelated\ProductCollection\Handlers\Ids</item>
                <item name="select_attributes" xsi:type="object">Rocket\AutorelatedProducts\Model\Indexer\Autorelated\ProductCollection\Handlers\SelectAttributes</item>
                <item name="visibility" xsi:type="object">Rocket\AutorelatedProducts\Model\Indexer\Autorelated\ProductCollection\Handlers\Visibility</item>
 
                <!--Out of stock filter-->
                <item name="outOfStock" xsi:type="object">Rocket\AutorelatedProducts\Model\Indexer\Autorelated\ProductCollection\Handlers\OutOfStock</item>

            </argument>

        </arguments>

### 3. Do a short explanation of the Composite pattern in general - usage, benefits, technical must and should…
It allows to treat objects of different types in the same structure and thus to be able to work with it as if it were a single object by implementing the same interface. In this way, whoever uses it does not have to worry about the implementation of each child, but they themselves define their own behaviour. 
- It follows the open-closed principle
- It is more maintainable, as having the logic separated allows for better debugging and earlier detection of a possible failure.
- Easier to test.
- Easier to work with complex data structures

Translated with www.DeepL.com/Translator (free version)