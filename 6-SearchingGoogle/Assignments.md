# A 
```
<?php

/*add **\Magento\Eav\Model\Config** in your construct method*/

protected $eavConfig;

public function __construct(
    \Magento\Eav\Model\Config $eavConfig
){
    $this->eavConfig = $eavConfig;
}

/*Now you can get attribute data using*/

$attribute = $this->eavConfig->getAttribute('catalog_product', Your_Attribute_Code);

echo "<pre>"; print_r($attribute->getData()); exit;
```
# B
```
<?php
 
public function __construct(
    \Magento\Eav\Model\Entity\Attribute $entityAttribute
) {
    $this->entityAttribute = $entityAttribute;
}
 
/**
* Load attribute data by code
* @return  \Magento\Eav\Model\Entity\Attribute
*/
public function getAttributeInfo($attributeCode)
{
    return $this->entityAttribute->loadByCode('catalog_product', $attributeCode);
}
```

# Assignments

### 1- In both cases, if you choose to inject any of the two proposed classes, which principle(s) get broken?

- Dependency inversion principle

### 2- What is a good way to use any code from a given module (Magento or any other third party one)?
- Use the abstractions defined in that module to get the request data or make the request functionality.

### 3- Examine the Magento Eav module and propose Option C to load attribute data
```
<?php

use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;
 
public function __construct(
    AbstractAttribute $entityAttribute
) {
    $this->entityAttribute = $entityAttribute;
}
 
/**
* Load attribute data by code
* @return  AbstractAttribute
*/
public function getAttributeInfo($attributeCode)
{
    return $this->entityAttribute->loadByCode('catalog_product', $attributeCode);
}
```